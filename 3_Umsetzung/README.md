# Umsetzung

## Lektionen: 

* Präsenz: 40
* Virtuell: 30
* Selbststudium: 20

*Hier können grössere Arbeiten aufgelistet werden, z.B. 35 Stunden u.a. für eine Übung GS-Würfel als Hausaufgabe*

## Modulstufe: 

*Wenn sich ein Modul in mehrere Untermodule unterteilt, stehen hier die Untermodule mit dem jeweiligen Semester. Ansonsten nur das Modul inkl. Semester*

1) UP1 im 3.Semester und 
2) UP2 im 4. Semester

## Bereich: 

keine Differenzierung

## Voraussetzungen:

*Hier sind evtl. Voraussetzungen aufgeführt. Vorzugsweise andere Module*

## Technik: 

*Falls nötig stehen hier technische Einrichten die benötigt werden, z.B. GSN3 Labor, pro Gruppe ein Rack im HF Labor, AWS Academy Kursumgebung, Azure Zugang*

## Methoden: 

*Ersetzen mit der verwendeten Methode, z.B. Scrum, ibo-Gesamtmodell der Organisation *

## Schlüsselbegriffe: 

Unternehmensprozesse:
-	Würfel nach Götz Schmid, Beziehungen, Aufbau-und Ablauforganisation, Elemente, Dimensionen
Projektmanagement:
-	Projektmanagement, Managementtechniken, Pflichtenheft/Evaluation, Projektarbeit, Methoden, Techniken im Projekt, Change Management, Projektphasen, Planungszyklus, Systems Engineering, der Mensch im Projekt, Projektführung, Zeitplanung

## Lehr- und Lernformen: 

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

*weitere...*

## Lehrmittel:  

*Link auf das eigene Repository*
*Links oder Erwähnung von externen Lernmitteln, z.B. https://agilemanifesto.org/ etc.)

## Hilfsmittel:

*Ersetzen mit formalen Hilfsmitteln, z.B.*

* Rahmenlehrplan 



