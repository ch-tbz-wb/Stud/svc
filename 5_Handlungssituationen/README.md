# Handlungsziele und Handlungssituationen 

Zu jedem einzelnen Handlungsziel aus der Modulidentifikation wird eine realistische, beispielhafte und konkrete Handlungssituation beschrieben, die zeigt, wie die Kompetenzen in der Praxis angewendet werden.

### 1. abc

Bsp für Handlungssituation: ich kann ...

### 2. def

Bsp für Handlungssituation: ich kann ...
