![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# Name des Moduls inkl. Kürzel

## Kurzbeschreibung des Moduls 

*Ersetzen mit einer griffigen Beschreibung, z.B. direkt aus dem Rahmenlehrplan*

## Unterrichtsressourcen

*Ersetzen mit Titel logisch zusammenhängender Unterrichtsressourcen*
* [Unterrichtsressource A](2_Unterrichtsressourcen/A)
* [Unterrichtsressource B](2_Unterrichtsressourcen/B)

*Ersetzen mit Titel logisch zusammenhängender Unterrichtsressourcen*
* Unterrichtsressource C

## Angaben zum Transfer der erworbenen Kompetenzen 

*Ersetzen mit einer Beschreibung wie der Transfer erfolg, z.B.- Umsetzung der erlernten Theorie in Einzel- und Gruppenarbeiten*

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

*Module aufführen (Nummer und Bezeichnung), welche abgeschlossen sein müssen, bevor mit diesem Modul begonnen wird.*

### Nachfolgende Module

*Module aufführen (Nummer und Bezeichnung), welche erst nach diesem Modul starten sollten, wo also dieses Modul eine Basis dazu bildet.*

## Dispensation

*Ersetzen mit Vorkursen oder Diplomen die mit dem Modul gleichwertig sind, z.B. Ausbildung zum Projektmanager*

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
